---
title: "About"
date: "2021-3-1"
---

# Hi there

I am a 15yo boy, who have just got a month older by the time I am writing this. Linux Affairs is meant to bring the latest and greatest Linux content and popularize the same among the masses... 

I have always had this mystical bond with Operating Systems, especially when it comes to tweaking them. Back in the day, Android was the first OS that caught my interest. It was extremely cuztomizable and versatile ( and it is ! ). After skipping a few, I stopped. Where? Linux, of course! At first, Linux was overwhelmingly difficult. I would wonder why would anyone use this shit. But then, I realised the beauty of this simplicity and versatility Linux offers. It was the most customizable thing ever! And at the same time, challenging. Now that it all started to make sense ( a little bit :). Still a long long way to go! ), I felt that I could write about my journey covering every impediments I came across while shifting to Linux, how I reacted and how I managed to fix em'. Well, this is indeed the reason why I started Linux Affairs - to help people move to linux. It's not just about sharing customization tutorials, but also "How to get started" and "How to fix {this and that}".


Okay, now that you know a bit about me and Linux Affairs, I want you to pardon me if I have/had made any mistakes in the past or in the coming future. I keep reviewing what I have written, and try I update the same. 
