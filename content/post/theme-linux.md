---
title: "The Most Beginner-Friendly Way to Theme Linux" 
date: 2021-05-10
tags: 
- the ocs-url way to theme linux
- linux themes
- easiest way to theme linux
- linux theming for beginners 
- theme gnome, kde, xfce, lxqt, lxde 
- how to use ocs-url

categories: 
- novice-me
- tutorial
- tips&tricks

description: "The easiest way to theme Linux"

toc: true
---

# Hi there

Wondering how to get rid of the hassle of downloading, extracting, copying and then pasting just to apply themes and icon packs. I got a solution for ya! Let me take you straight into the steps...

# The Steps



## 1. Install the Dependencies

  
- #### For Ubuntu users
  ```sh
  $ sudo apt install libqt5svg5 qml-module-qtquick-controls
  ```

- #### For Fedora users
  ```sh
  $ sudo dnf install qt5-qtbase qt5-qtbase-gui qt5-qtsvg qt5-qtdeclarative qt5-qtquickcontrols
  ```

  
- #### For OpenSuse users
  ```sh
  $ sudo zypper install libQt5Svg5 libqt5-qtquickcontrols
  ```

  
- #### For ARCH users 
  ```sh
  sudo pacman -S qt5-base qt5-svg qt5-declarative qt5-quickcontrols
  ```


## 2. Install the `ocs-url` package


-  ### For Ubuntu users
   Download [here](https://www.opendesktop.org/p/1136805/)

   The file should be located on `files tab >> ocs-urlxxx.deb`

   Install using:
   ```sh
   $ sudo dpkg -i /path/to/ocs-url*.deb
   ```

-  ### For Fedora users
   Download [here](https://www.opendesktop.org/p/1136805/)

   The file should be located on `files tab >> ocs-urlxxx.rpm`

   Install using:
   ```sh
   $ sudo rpm -i /path/to/ocs-url*.rpm
   ```
  
-  ### For OpenSuse users
   Download [here](https://www.opendesktop.org/p/1136805/)

   The file should be located on `files tab >> ocs-urlxxx.rpm`

   Install using:
   ```sh
   $ sudo rpm -i /path/to/ocs-url*.rpm
   ```

-  ### For Arch Users
   Download [here](https://www.opendesktop.org/p/1136805/)

   The file should be located on `files tab >> ocs-urlxxx.pkg.tar.xz`

   Install using:
   ```sh
   $ sudo pacman -U /path/to/ocs-url*.pkg.tar.xz 
   ```


## 3. Apply a theme/icon pack!

  ### 3.1 Search for any theme in the search box [here](https://www.opendesktop.org/browse/) 
  
  For example, "GTK3 themes"

  ### 3.2 Click on install >> install with "ocs-url" (small text below install)
  
  Wait for ocs-url to load

  ### 3.3 A pop-up should appear
  
  Just follow the on-screen instructions

  ### 3.4 Download should start with an ocs pop-up screen
  
  It will automatically place the theme into .themes or icon into .icons

  ### 3.5 Now you can apply your theme in your system-settings


  
  *Thank you for reading!* :-)






