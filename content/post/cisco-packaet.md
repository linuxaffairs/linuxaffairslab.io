---
title: "Install Cisco Packet Tracer on Arch Linux"
date: 2021-05-16
tags: 
- Cisco Packet Tracer
- Networking tools
- How to install cisco packet tracer on Arch Linux
- Packet tracer installation guide
- Packet tracer install on Linux
- Cisco Network simulation on Linux

categories: 
- tutorial

description: "Packettracer on Arch Linux"
---

# Hi there

A network tech? Plus, you are on Arch Linux and no updated guide on how to install the cisco packet tracer? You have come to the right place! Becuase, pacman doesn't have any such packages named `packettracer` (atleast in your local repo) and the AUR's version is giving some strange errors during the build process. 

There is one step that people miss while trying to fetch it directly from AUR. Though packet tracer is free, you need to first agree to the *CISCO END USER LICENSE AGREEMENT (“EULA”).* For that, you will first need to **[sign up](https://www.netacad.com/)** with Cisco's Network Academy ( which is absolutely free! ).

By now, I assume that you have successfully signed up with [Netacad](https://www.netacad.com/) and ready to install it. Let's go...

## 1. Download the Packet Tracer

Download the Linux Desktop version from [here](https://www.netacad.com/portal/resources/packet-tracer).

## 2. Clone `packettracer` from AUR

```sh
git clone https://aur.archlinux.org/packettracer.git
```

## 3. Copy the .deb package from step 1 to the cloned repo

Do this:

```sh
cp -r ~/Downloads/PacketTracer_xxxx.deb ~/packettracer/
```

## 4. Easy from here

```sh
cd ~/packettracer/
```

```sh
makepkg -si
```

## 5. Done.

You should see Packet Tracer in your launcher menu.
