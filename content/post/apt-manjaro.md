---
title: "Why is apt-get not working on Manjaro, Arco, or Garuda Linux?" 
date: 2021-05-10
tags: 
- apt-get install for Manjaro
- apt-get update not working
- package manager in Manajaro, Arco, Garuda, etc.
- command to install packages in Manjaro
- apt-get command not found in Manjaro Linux
- a different apt in arch linux

categories: 
- novice-me

description: "That's me messing around with Arch"

---

# Hi there

Well, if you have the same question, then you're an absolute me! (lol) Don't worry, the answer isn't far away from you.

# The Why

It is simply because these distros - Manjaro, Arco Linux, Garuda Linux, Reborn os, etc. - are build on top of [Arch Linux](https://archlinux.org/about/). What then? Well, all the Arch based distros and Arch itself (of course), uses a different way of managing packages or software aka installing or removing packages. So, Debian and Ubuntu's package management system is called apt and Arch's package management system is called pacman ( remember the retro game? ). Similary, Fedora uses something called dnf and Solus uses eopkg for the same.

Therefore, to install packages on Manjaro, Garuda or any other Arch based distro, you need to type:
```sh
sudo pacman -S <name of the package you want to install>
```
Here `-S` means *sync*. Similar to `apt-get install` in Ubuntu or Debian. *( Just a fancy way of doing stuff !)*

To remove a package, simply type:
```sh
sudo pacman -R <name of the package you want to remove>
```



