---
title: "Kick-start your Linux Journey" 
date: 2021-05-11
tags: 
- fundamentals of an operating system
- path to linux mastery
- how should you start with linux
- linux for newcomers
- fundamentals of linux
- linux journey 

categories: 
- tips&tricks

description: "Here's how you should actually start your Linux journey"

toc: true
---

# Hi there

First and foremost, do NOT get overwhelmed by thinking that Linux is too sophisticated and made for nerds only. None of them makes sense unless you really get into messing with the kernel and other stuff. Desktop Linux has gained popularity in the last few years. Pop OS, Manjaro, Linux Mint are really made for desktops. They just work! Seamlessly out of the box! Well, they are not the only ones in the game. That's what we'll discover. 

## What you should expect

Since you're almost ready to start off your Linux journey, you're bound to come across those nasty errors, system failures and all the other messy stuff in the paradise (but you are unlikely to face any them if you don't want to explore and learn hihi ).

## 1. Here's the first thing you should "watch" NOT "do" >.<

I would first like to convince you enough for why you should use Linux. Click on these links below cause I want to please your eyes a bit ;-) 

**[#sudo](https://www.youtube.com/watch?v=uucr0xHoz1A)**

**[Distro Tube](https://www.youtube.com/watch?v=pY9HPm1N16Y)**

**[Unix Porn](https://www.reddit.com/r/unixporn/)**

**[Instant OS](https://www.youtube.com/watch?v=z10s_3E6fgY)**

Saw em? Don't you think they seem like they have more control over their desktops? Sure they do because it's all about Linux! Linux brings you everything that you never thought you could with any other operating system. 

And here's the fun part. Now, as you have seen the above videos and pictures from Unix porn, you might also want your desktop look something like them if not identical. Then do it! It's not difficult to do so, all you need is patience and a clear goal. In the path of making your desktop look like them, you'll learn much more than any tutorial that can ever teach you ( cuz now you have a clearer goal ). You are just a search away. Go and search for **"How to theme Linux"** or **"How to rice Linux"**.

## 2. My recommendation on how to start.

Before theming or anything, the most important thing you should and must do is learn how to use the **Terminal** ( or it would not be too late for you to get frustrated and eventually give up ). To learn the terminal, the best place to go to ( according to me ) is the website called **[Linux Journey](https://linuxjourney.com/)**. It is by far, would be my best recommendation to anyone. It takes you through the most important topics that every Linux user should know. It might not go into the depths, but it is the bare minimum. So, before continuing, be sure to give it a try!

## 3. There's a course on udemy ( not necessary, but really good )

There's a course on udemy called **Linux Mastery** by **Ziyad Yehia** who is a very engaging intructor. Meaning, he won't let you get bored while learning how to use the terminal. He covers almost the fundamentals of the shell commands ( and the rest depends upon you of course). I bought the course for about $5 on udemy, which was absolutely worth it!

*None of the people that I will or have mentioned on my post, have sponsored me. All the suggestions are based upon my personal experiences*

**[Link to the course](https://www.udemy.com/course/linux-mastery/)**


- ### 3.1. A free alternative which is equally good!

Hey mate, don't get upset for suggesting a paid option. There is an equally good alternative as well. Check out the playlist made by **Learn Linux TV**. 

**[Link to the course](https://www.youtube.com/playlist?list=PLT98CRl2KxKHaKA9-4_I38sLzK134p4GJ)**

## 4. You should now be good to go.

You can now follow these youtube channels that post really wonderful Linux content ( It would be difficult to cover all of them cause there are many by now. Its 2021 guys, and Linux has already started to bloom :-) ) 

**[The Linux Cast](https://www.youtube.com/channel/UCylGUf9BvQooEFjgdNudoQg)**

**[Tech Hut](https://www.youtube.com/channel/UCjSEJkpGbcZhvo0lr-44X_w)**

**[Switched to Linux](https://www.youtube.com/c/SwitchedtoLinux/videos)**

**[Learn Linux TV](https://www.youtube.com/watch?v=DPLnBPM4DhI)**

**[Broodie Robertson](https://www.youtube.com/user/OmegaDungeon)**

**[Distro Tube](https://www.youtube.com/channel/UCVls1GmFKf6WlTraIb_IaJg)**

**[Mental Outlaw](https://www.youtube.com/channel/UC7YOGHUfC1Tb6E4pudI9STA)**

**[EF - Linux Made Simple](https://www.youtube.com/channel/UCX_WM2O-X96URC5n66G-hvw)**

Remember there are many equally good channels on youtube and you would surely come across them as you search for Linux related content.


*Thank you for reading and keep learning*
