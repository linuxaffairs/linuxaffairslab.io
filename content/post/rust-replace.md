---
title: "Replace Your GNU Utilities With These..."
date: 2021-05-21
tags: 
- rust programs
- rust replacement for gnu utilities
- exa, procs, ripgrep, fd, bat, procs, goping
- make ls, cat, ps commands look better
- best rust replacement for C programs
- rust lang programs for linux
- rusty software for linux
categories: 
- tips&tricks
description: "Some FOSS rust alternatives for GNU utilities"

---

# Hi there

You know, rust is a meme language. That's what I like about it. It's exciting when a meme creates such a hype that eventually turns it into a product. Ah I know! Its not a product! But a cool looking suite of Rust programs. So, here are a few rust programs that can potentially replace those good old C programs, which have been there since ages. 

## 1. `cat` ---> `bat`

`bat` is a rust program which replaces `cat`. In case you don't know what it is; It is a program that prints the content of a file on the **standard output**. Here, standard output is tty. Meaning, the content will be displayed on your terminal itself.

**Syntax:** `bat [OPTIONS] [FILE]...`

**Example:** `bat .zshrc` (with no options specified)

## 2. `ls` ---> `exa`

`exa` is an rust alternative for the `ls` command (if you aren't aware of this, go use windows....just kidding >.<) -- used to display contents in a directory.

## 3. `ps` ---> `procs`

`procs` is a rust replacement for the ps command -- used to display all the  current running processes. There are way to many options in ps to list them all. I am used to the BSD syntax --  `ps ax` or `ps axu` to list every process in your system. But for procs, there aren't many...You don't even need to use any options with procs. Just type in `procs` to list all the running processes in your system.

## 4. `grep` --> `ripgrep` (rg)

Similarly, `ripgrep` replaces grep, to which you are used to. It can search for patterns in whichever input you feed it with. Got it?

**Syntax**: rg [OPTION...] PATTERNS [FILE...]

**Example**: rg alias .bashrc

## 5. `find` --> `fd`

The almighty `find` command can be replaced with `fd`. As the name suggests, it searches for files in a specified directory.

**Syntax**: fd [OPTION..] PATTERNS [PATH..]

**Example**: fd -t f .jpg /home/$USER/ 
( Here, -t is a option that I used to specify the type. Meaning, I want to search for files ending with '.jpg' in path /home/$USER/)

*Hope you enjoyed reading*



