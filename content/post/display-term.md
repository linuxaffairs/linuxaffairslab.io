---
title: "How do you display something in the terminal as you start it?" 
date: 2021-05-10
tags: 
- cool texts as the terminal starts
- display system information in the terminal
- make the terminal look cool
- display images in terminal start-up
- stylize the terminal
- make neofetch start as the terminal starts 

categories: 
- novice-me
- tutorial

description: "That's me trying to look cool"

---

# Hi there

Have you come here for the exact same reason? If yes, give me a high five!! Because that's what I was searching for too. I always wondered "How do these guys make the terminal pop-up some cool texts and images as it starts". So, let's dive into what makes the terminal do these cool stuff.

# The Shell

All the magic startes here my friend! Your terminal is just the window where you type in text. The thing that actually executes the commands that you type in, is the so called **Shell**. The shell is what *interprets* the commands. Therefore, your shell is what starts as you open the terminal.

Now, open up your terminal window and type in the following command to know which shell you're using:
```sh
echo $SHELL
```
- If it shows `/bin/bash`, then you are using the `bash` Shell
- If it shows `/bin/zsh`, then you are using the `zsh` Shell
- If it shows `/bin/fish`, then you are using the `fish` Shell

> All of them are almost equally good, some like the fish shell carry more features by default

# I'll make you look cool now

All the shells must have a configuration file where you can put in various stuff to make it your own. For example:

- fish config file is located in: `~/.config/fish/config.fish`
- zsh config file is located in `~/.zshrc`
- bash config file is located in `~/.bashrc`

Now, just edit them to display cool texts in you terminal because **remember! YOUR SHELL STARTS AS THE TERMINAL WINDOW STARTS).**

We now need a software that can display these texts ( there are many, just search for them. For now, I will use a package called `neofetch` )

UBUNTU users type in:
```sh
sudo apt install neofetch
```
ARCH Linux users type in:
```sh
sudo pacman -S neofetch
```

> For other linux distros that I haven't covered here (Since there are way too many), just search "How to install neofetch in <the distro you're using> in your internet browser"

Now that you have `neofetch` installed, it's easy...

Just edit the the files`~/.bashrc`, `~/.zshrc` or `~/.config/fish/config.fish` ( according to the shell you're using)

Just do:
```sh
sudo nano ~/.bashrc
```
**OR**
```sh
sudo nano ~/.zshrc
```
**OR**
```sh
sudo nano ~/.config/fish/config.fish
```

And then, type `neofetch` at the end of config file of the Shell you're using. If you are using bash shell then type neofetch at the end of all the lines in `.bashrc`. Do accordingly for the other shells.

Congrats! You have just made your terminal look cooler. Now everytime you open a new terminal window you should see something like ( looks different on different distros...)

```sh
                   ./o.                  mdx@vbook
                   ./sssso-                ---------
                 `:osssssss+-              OS: EndeavourOS Linux x86_64
               `:+sssssssssso/.            Host: VivoBook_ASUSLaptop X412FJC_
             `-/ossssssssssssso/.          Kernel: 5.11.16-arch1-1
           `-/+sssssssssssssssso+:`        Uptime: 2 hours, 23 mins
         `-:/+sssssssssssssssssso+/.       Packages: 981 (pacman)
       `.://osssssssssssssssssssso++-      Shell: zsh 5.8
      .://+ssssssssssssssssssssssso++:     Resolution: 1920x1080
    .:///ossssssssssssssssssssssssso++:    DE: GNOME 40.1
  `:////ssssssssssssssssssssssssssso+++.   WM: Mutter
`-////+ssssssssssssssssssssssssssso++++-   WM Theme: Adwaita
 `..-+oosssssssssssssssssssssssso+++++/`   Theme: Adwaita [GTK2/3]
   ./++++++++++++++++++++++++++++++/:.     Icons: Adwaita [GTK2/3]
  `:::::::::::::::::::::::::------``       Terminal: alacritty
                                           CPU: Intel i5-10210U (8) @ 4.200GH
                                           GPU: Intel UHD Graphics
                                           GPU: NVIDIA GeForce MX230
                                           Memory: 2614MiB / 7790MiB

```

Since I am using endeavour OS, my neofetch looks like above :-)

