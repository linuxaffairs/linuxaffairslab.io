---
title: "Update-grub Equivalent for Fedora"

date: 2021-05-08

author: Anonymous

tags: 
- windows disappeared after installing fedora
- make grub config file on fedora
- update-grub for fedora
- what is update-grub2 for fedora 
- boot menu disappeared 
- recover boot menu

categories: 
- tips&tricks
- tutorial

description: "update-grub and update-grub2 equivalent on fedora"

toc: true
---

# Hi there

Don't panic if you don't know why your boot menu disappeared after installing fedora. You are just a few commands away. Btw, you might already know the `update-grub` or `update-grub2` from Ubuntu. But, it is a pretty short command for a slightly complex task. Also, we'll do one more step prior to making the grub config file ( just to be on the safer side :-) ). Let's get straight into into it!

## The Steps


### 1. Upgrade your system by typing in ( in your terminal ;-) )

```sh
sudo dnf update
```

### 2. Install `os-prober`

Why? `os-prober` searches for other OSes which might be installed on your system ( which should be windows is this case ).

```sh
sudo dnf install os-prober
```

### 3. Run `os-prober`

```sh
sudo os-prober
```

### 4. Make the grub config ( IN UEFI SYSTEMS OR BIOS SYSTEMS )

#### 4.1 Check if your system is UEFI or BIOS

+ Become a super user
```sh
su
```
+ Check for the file
```sh
ls -ld /sys/firmware/efi
```

*If you get something “No such file or directory” error, then your system is using BIOS. ( for BIOS, just follow up!)*

#### 4.2 Now, if you have the file, you have support for UEFI. So, you can execute the line below...
  ```sh
  sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
  ```

#### 4.2.1 If you don't have the file, you're on legacy BIOS. So, execute the line below...

  ```sh
  grub2-mkconfig -o "$(readlink -e /etc/grub2.conf)"
  ```	

### 5. Reboot!

```sh
sudo reboot
```

#### The boot menu should appear now. Thank you for bearing with me...
