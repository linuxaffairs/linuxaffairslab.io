---
title: "Spicing Arch Linux Based Distros with Arco Linux Spices Application"
date: 2021-05-18
tags: 
- add third party repo in arch linux
- custom bashrc
- arco linux repos in other arch based distros
- aliases
- arco linux spices application
- make arch linux arco linux
- arco linux third party repository
categories: 
- novice-me
- tips&tricks
description: "Turn any Arch Linux based distribution into Arco Linux"
---

# Hi there

I know your affection towards Arco Linux even though you're on a diferent distribution. Hats off to the developer of Arco Linux **Erik Dubois** who is such a hard-working person and really supportive to other members as well. Coming to me, I use Endeavour OS ( another great Arch based distro ), but I am still using all the **aliases** from Arco Linux. Alongside endeavour os repos, I have also got all the **repos** from Arco Linux. Meaning, I have spiced Endeavour OS with Arco Linux.

For the ease of adding Arco Linux goodness any arch based distro, Erik has made a really useful gui appllication named **Arco Linux Spices Application**. It makes adding arco repos a whole lot easier because you don't manually need to install Arco Linux **keyring** and **mirrorlist** to add the repo. Also, it will automatically add all the signatures and stuff from Arco Linux.

## Download

Download the Arco Linux Spices Application from [here](https://www.arcolinux.info/download/arcolinux-spices-application/).

## Install

Install the downloaded package using:
```sh
sudo pacman -U ~/PATH/TO/ARCO-LINUX-SPICES-APPLICATION
```
# Done

Click on the various options to add repos, bashrc from arco linux, etc.
