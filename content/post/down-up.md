---
title: "Error: could not register 'arcolinux_repo' database (database already registered)"
date: 2021-05-18
tags: 
- pacman error
- database already registered fix
- could not add repos in arch linux
- yay autosuggestions not working
- could not register repo fix
- repository errors arco linux

categories: 
- fix
description: "Fix the repo error called 'database already registered'"
---

# Hi there

This error occurs when you accidently add a repository **multiple times**. As a result, two or more repos at get listed in `/etc/pacman.conf`. So, the fix should be obvious now.

## Fix

### 1. Edit the `pacman.conf` file

```sh
sudo vim /etc/pacman.conf
```

### 1.1 Delete

delete any cloned repo in the file!
