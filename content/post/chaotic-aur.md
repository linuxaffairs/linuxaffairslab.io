---
title: "Add Chaotic-AUR Repo in Arch Linux"
date: 2021-05-22
tags: 
- custom repo arch linux
- how to add chaotic aur repo in arch linux
- use chaotic repository on arch linux
- chaotic-aur repository
- keyring and mirrorlist
- unofficial repository for arch linux
- add more repositories in arch linux
- add chaotic-aur to pacman.conf
categories: 
- tips&tricks
- tutorial
description: "Adding the chaotic-aur repository in arch linux"
---

# Hi there

Today we'll see how to add chaotic-aur repo in arch based distros and arch itself, of course! Chaotic-aur is a unofficial user repo that has prebuilt binaries from the AUR. You get a large suit of binaries to install. Let's see how do we actually add it to pacman:

## 1. Import and sign the public key

- #### Import

 ```sh
 sudo pacman-key --recv-key 3056513887B78AEB
 ```

- #### Sign locally

```sh
sudo pacman-key --lsign-key 3056513887B78AEB
```

## 2. Install the keyring and mirrorlist

```sh
sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-'{keyring,mirrorlist}'.pkg.tar.zst'
```

## 3. Add this to the end of the file ( or simply, append ) `/etc/pacman.conf`

```sh
[chaotic-aur]
Include = /etc/pacman.d/chaotic-mirrorlist
```

## 4. Done!

Update the pacman database

```sh
sudo pacman -Syy
```

*Hope you enjoyed reading*
