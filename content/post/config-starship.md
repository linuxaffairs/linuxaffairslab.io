---
title: "Configure the Starship Shell Prompt"

date: 2021-05-06

author: Anonymous

tags: 
- starship shell prompt
- custom prompts for linux
- featurefull prompts
- configure the prompt
- configure starship prompt
- make prompt look good 
- how to change the default prompt

categories:
- configuration 
- tutorial

description: "Configure the starship shell prompt in the easiest way possible!"

toc: true

---

# Hi there

I won't waste your time here. Let me take straight into the configuration file of the almighty starship shell prompt. 

>**NOTE**: *I assumed that you already know what a prompt is and most importantly the starship shell prompt or you would have not searched about configuration if you didn't know ! :)*

## 1. Create the Configuration file - SKIP if you have it already

All configurations for starship is done in `~/.config/starship.toml`

If you don't already have it, just create one! - by executing the following lines in your terminal.

```sh
$ cd ~
$ touch .config/starship.toml
```

## 2. Configuring the `.toml` file

Configuring a `.toml` file is painless and straightforward!

If you already have the file `~/.config/starship.toml`, you should see something like this:

```sh
# Don't print a new line at the start of the prompt
add_newline = false

# Make prompt a single line instead of two lines
[line_break]
disabled = true

# Replace the "❯" symbol in the prompt with "➜"
# [character]                         # The name of the module we are configuring is "character"
# success_symbol = "[➜](bold green)"  # The "success_symbol" is set to "➜" with color "bold green"

# Use custom format
# format = """
# [┌───────────────────>](bold green)
# [│](bold green)$directory$rust$package
# [└─>](bold green) """

# Disable the package module, hiding it from the prompt completely
[package]
disabled = true
```

You see them? A hell lot of comments or `#` in front of most lines. Means, they won't get executed. ( *Useless shit XD* ) 


>Now, if you have this empty, don't worry mate! Just bear with me. Just copy and paste the above lines to `.config/starship.toml`

ALright! Now we have it all. Let's take our time configuring it ( which is as easy as ABD *XD* ) 






#### 2.1 If you want your prompt to be in two lines instead of one, change disbled from `true` to `false`:

```sh
# Make prompt a single line instead of two lines
[line_break]
disabled = false
```

#### 2.2 If you want to replace the "❯" symbol in the prompt with "➜", uncomment the lines `[character]` and `success_symbol`

```sh
[character]                         # The name of the module we are configuring is "character"
 success_symbol = "[➜](bold green)"  # The "success_symbol" is set to "➜" with color "bold green"
```

#### 2.3 If you want to make your prompt look something like this:
 ```sh
~ on  master [+?]  ┌───────────────────>
 │
 └─>  
 ```
 Just uncomment the following lines and save the file. You're the done...

 ```sh
format = """
 [┌───────────────────>](bold green)
 [│](bold green)$directory$rust$package
 [└─>](bold green) """
 ```



Congratulations! You've just added some makeup to your ugly prompt..
Thank you and *keep learning*

*video reference* [link](https://www.youtube.com/watch?v=h8YWjUOZuLg)
