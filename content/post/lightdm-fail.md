---
title: "Failed to Start Light Display Manager" 
date: 2021-05-15
tags: 
- failed to start lightdm.service
- arco linux failed to start light display manager
- display manager issues
- lightdm giving errors
- lightdm not starting in arco linux
- skel 
- sudo systemctl start/stop lightdm.service

categories: 
- fix

description: "Fix lightdm errors on startup"
---

# Hi there

I have faced this error a multiple times in the past whenever I chose to use lightdm as my display manager in Arch Linux. The solution to this problem has been referenced from [Erik Dubois's videos](https://www.youtube.com/results?search_query=failed+to+start+lightdm+erik+dubois). Just follow me through the steps...

*Not sure if this happens in debian, ubuntu and other distributions as well. It is completely based on my experience.*


## 1. Access your shell

Once, you recieve the error, you don't need to create a live usb and boot to another OS. You can still access your shell and execute commands. For this, in the error screen, just press `ctrl + alt + F1` or `ctrl + alt + F2`

## 2. Update your system

Now, update your system by typing in:

```sh
sudo pacman -Syu
```

## 3. Stop and then Start the lightdm.service

Execute the following below:

#### 3.1 Stop

```sh
sudo systemctl stop lightdm.service
```

#### 3.2 Start

```sh
sudo systemctl start lightdm.service
```

### 4. You should be able to boot now.

*Thank you*





