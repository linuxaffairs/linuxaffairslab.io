---
title: "The Best Way to Create a Windows Bootable USB in Linux"
date: 2021-05-23
tags: 
- how to make a windows bootable usb stick in linux
- dual boot windows and linux
- burn a usb with windows on linux
- flash windows in usb stick
- best way to make a windows bootable usb stick
- windows and arch linux dual boot
- copy and paste contents from windows iso not working
- windows and linux dual boot failed
categories: 
- tutorial
description: "The one and only working way to create a windows bootable usb stick in linux"

toc: true
---

# Hi there

Out of many ways to create a windows bootable usb stick in Linux, the only thing that worked for me was Woe Usb. It is a great tool to flash, specifically windows. It is available for almost any major Linux distribution. In case it's not your existing repo, u can install it as a snap package.

## 1. Install WoeUSB

- ### For Arch Linux Users

 ```sh
 yay -S woeusb-gui
 ```

- ### For Ubuntu Users

  #### 1.1. Add WoeUSB repo
  ```sh
  sudo add-apt-repository ppa:nilarimogard/webupd8
  ```
  #### 1.2. Update

  ```sh
  sudo apt update
  ```
  #### 1.3. Install WoeUSB

  ```sh
  sudo apt install woeusb
  ```

- ### For Fedora Users

  ```sh
  sudo dnf install WoeUSB
  ```
> *NOTE: For all the left-out distros, please give it a quick internet search on "how to install woeusb in...."*

## 2. Flash Windows using WoeUSB

### 2.1. Choose a Disk Image

CLick on the folder icon next to none to select the windows iso you want to burn

### 2.2. Choose the Filesystem

Since Windows uses a NTFS as its file system, just choose NTFS!

### 2.3 Choose the Target Device

This refers to your usb stick that you'll be flashing...

### 2.4 DONE!

Just wait until the process completes. Reboot your system, boot to BIOS and then choose the usb stick with windows.

*Hope you enjoyed reading*
